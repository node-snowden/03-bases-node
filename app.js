const {createFile} = require('./helpers/multiply');
const colors = require('colors');
const argv = require('./config/yargs');
console.clear();


createFile(argv.b, argv.l, argv.t)
	.then(nameFile => console.log(nameFile.rainbow, 'created'))
	.catch(err => console.log(err));
