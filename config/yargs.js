const argv = require('yargs')
	.option('b', {
		alias: 'base',
		type: 'number',
		demandOption: true,
		describe: 'Is the base of multiply table.'
	})
	.option('l', {
		alias: 'list',
		type: 'boolean',
		default: false,
		describe: 'Show the table in console'
	})
	.option('t', {
		alias: 'to',
		type: 'number',
		default: 10,
		describe: 'Number up to the table is displayed'
	})
	.check((argv, options) => {
		if (isNaN(argv.b)) {
			throw 'The base must be a number.';
		}
		return true;
	})
	.argv;

module.exports = argv;

