const fs = require('fs');
const colors = require('colors');

const createFile = async (base = 5, list, to = 10) => {

	try {

		let output, consol = '';

		for (let i = 1; i <= to; i++) {
			output += `${base} x ${i} = ${base * i}\n`;
			consol += `${base} ${'x'.green} ${i} ${'='.green} ${base * i}\n`;
		}

		if (list) {
			console.log("======================".green);
			console.log('Table of: '.green, colors.blue(base));
			console.log("======================".green);
			console.log(consol);
		}


		fs.writeFileSync(`./output/table-${base}.txt`, output);
		return `table-${base}.txt created`;
	} catch (err) {
		throw err;
	}
}

module.exports = {
	createFile
}
